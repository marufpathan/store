import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Item } from '../products/products.component';
import { CustomCurrencyPipe } from '../currency.pipe';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
  providers: [CustomCurrencyPipe],
})
export class CheckoutComponent implements OnInit {
  items: any;
  total: number = 0;
  cart: Item[] = [];
  selectedCurrency: string = 'INR';
  exchangeRate: number = 1;

  constructor(
    private router: Router,
    private cartService: CartService,
    private customCurrencyPipe: CustomCurrencyPipe
  ) {}

  ngOnInit() {
    this.items = this.cartService.getItems();
    this.cart = this.cartService['getCart']();
    this.updateTotal();
  }

  clearCart() {
    this.items = this.cartService.clearCart();
    this.updateTotal();
  }
  updateTotal() {
    this.total = this.customCurrencyPipe.transform(
      this.totalAmount() * this.exchangeRate,
      this.selectedCurrency
    );
  }

  totalAmount() {
    let total = 0;
    for (const item of this.items) {
      total += item.price * item.quantity;
    }
    return total;
  }

  removeFromCart(item: any) {
    this.cartService.removeFromCart(item);
    this.items = this.cartService.getItems();
    this.updateTotal();
  }

  addToCart(item: any) {
    this.cartService.addToCart(item);
    this.items = this.cartService.getItems();
    this.updateTotal();
  }

  incrementQuantity(index: number) {
    this.items[index].quantity++;
    this.cartService['setCart'](this.items);
    this.updateTotal();
  }

  decrementQuantity(index: number) {
    if (this.items[index].quantity > 1) {
      this.items[index].quantity--;
    } else {
      this.removeFromCart(this.items[index]);
    }
    this.cartService['setCart'](this.items);
    this.updateTotal();
  }

  setCurrency(currency: string) {
    this.selectedCurrency = currency;
    // based on currency needed
    this.exchangeRate = this.getExchangeRateForCurrency(currency);
    this.updateTotal();
  }

  private getExchangeRateForCurrency(currency: string): number {
    //Hardcoded can add API (need account and acces keys )
    if (currency === 'INR') {
      return 1;
    } else if (currency === 'USD') {
      return 0.84;
    } else {
      return 1;
    }
  }
}
