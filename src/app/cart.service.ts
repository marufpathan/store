import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  [x: string]: any;
  items: any[] = [];

  addToCart(item: any) {
    this.items.push(item);
  }

  getCartTotal(): number {
    let total = 0;
    this['cart'].forEach((item: { price: number; quantity: number }) => {
      total += item.price * item.quantity;
    });
    return total;
  }

  removeFromCart(item: any) {
    const index = this.items.indexOf(item);
    if (index > -1) {
      this.items.splice(index, 1);
    }
  }

  getItems() {
    return this.items;
  }

  clearCart() {
    this.items = [];
    return this.items;
  }

  constructor() {}
}
