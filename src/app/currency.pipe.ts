import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'customCurrency',
})
export class CustomCurrencyPipe implements PipeTransform {
  transform(
    value: any,
    currencyCode: string = 'USD',
    symbolDisplay: boolean = true,
    digits: string = '1.2-2'
  ): any {
    const currencyPipe = new CurrencyPipe('en-US');
    const formattedValue = currencyPipe.transform(
      value,
      currencyCode,
      symbolDisplay,
      digits
    );
    if (formattedValue == null) {
      return '';
    }
    if (currencyCode === 'INR') {
      return formattedValue.replace('$', '₹');
    } else {
      return formattedValue;
    }
  }
}
