import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from 'src/app/cart.service';

export interface Item {
  name: string;
  price: number;
  discount?: number;
  label?: string;
  imgUrl?: string;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit {
  title = 'ngrxdemo';

  /*OWL*/
  items: Item[] = [
    {
      name: 'Item 1',
      price: 10.99,
      discount: 2,
      label: 'Sale',
      imgUrl: '../assets/Images/Batman Tshirt.jpg',
    },
    {
      name: 'Item 2',
      price: 15.99,
      imgUrl: '../assets/Images/Batman Tshirt.jpg',
    },
    {
      name: 'Item 3',
      price: 8.99,
      imgUrl: '../assets/Images/Batman Tshirt.jpg',
    },
    {
      name: 'Item 4',
      price: 12.99,
      discount: 1,
      imgUrl: '../assets/Images/Batman Tshirt.jpg',
    },
  ];

  id = '';

  constructor(public cartService: CartService, private router: Router) {}

  ngOnInit() {}

  addToCart(item: Item) {
    this.cartService.addToCart(item);
  }

  removeFromCart(item: Item) {
    this.cartService.removeFromCart(item);
  }

  drop(value: any) {
    if (this.id === value) {
      this.id = '';
    } else {
      this.id = value;
    }
  }

  url: string = '../assets/Images/th.jpg';
  imageChange(event: any): void {
    const selectedImageUrl = this.items.find(
      (item) => item.imgUrl === event.target.src
    );
    this.url = event.target.src;
  }
}
