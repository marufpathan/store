import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  selectedCurrency = 'USD';
  items = [
    { name: 'Item 1', price: 10.99 },
    { name: 'Item 2', price: 15.99 },
    { name: 'Item 3', price: 8.99 },
    { name: 'Item 4', price: 12.99 },
  ];

  id = '';

  constructor(public cartService: CartService, private router: Router) {}

  ngOnInit() {}

  setCurrency(currency: string) {
    this.selectedCurrency = currency;
  }

  addToCart(item: any) {
    this.cartService.addToCart(item);
  }

  removeFromCart(item: any) {
    this.cartService.removeFromCart(item);
  }

  drop(value: any) {
    if (this.id === value) {
      this.id = '';
    } else {
      this.id = value;
    }
  }
  url: string = '../assets/Images/th.jpg';
  imageChange(event: any): void {
    this.url = event.target.src;
  }

  goToCheckout() {
    this.router.navigate(['/checkout']);
  }
}
